﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// USER
using System.Xml;

namespace myxml
{
    public class FeasAppConfigXml
    {
        string appl_name = "Feasiblity_Appl";
        public string gauge_x = "";
        public string gauge_y = "";
        public string gauge_roll = "";
        string xml_path = "";

        MyXml xFile;

        public FeasAppConfigXml(string path)
        {
            xml_path = path;
            xFile = new MyXml(xml_path);

            // Test Code
            updateAppData();

            //Console.WriteLine("WRITE VALUE");
            //Console.WriteLine("gauge_x");
            //Console.WriteLine(readNodeData("/appl_config/gauge_data/gauge_x"));
            //Console.WriteLine("gauge_y");
            //Console.WriteLine(readNodeData("/appl_config/gauge_data/gauge_y"));
            //Console.WriteLine("gauge_roll");
            //Console.WriteLine(readNodeData("/appl_config/gauge_data/gauge_roll"));

            //Console.WriteLine("REPLACE VALUE");

            //updateNodeData("/appl_config/gauge_data/name", "gauge_raw_position");
            //save();

            //Console.WriteLine("gauge_x");
            //string temp = Console.ReadLine();
            //updateNodeData("/appl_config/gauge_data/gauge_x", temp);

            //Console.WriteLine("gauge_y");
            //temp = Console.ReadLine();
            //updateNodeData("/appl_config/gauge_data/gauge_y", temp);

            //Console.WriteLine("gauge_roll");
            //temp = Console.ReadLine();
            //updateNodeData("/appl_config/gauge_data/gauge_roll", temp);
        }
        public void updateAppData()
        {
            string tNow = DateTime.Now.ToString();
            xFile.updateAttData("appl_config", "name", appl_name);
            xFile.updateAttData("appl_config", "modified", tNow);
        }
    }
    public class MyXml
    {
        public string xml_path = "";
        
        XmlDocument xDoc;

        public MyXml(string xml_path_load)
        {
            xml_path = xml_path_load;
        }
        public void read()
        {
            xDoc = new XmlDocument();
            xDoc.Load(xml_path);
        }
        public void save()
        {
            try
            {
                xDoc.Save(xml_path);
            }
            catch
            {
                
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node_path">Ex. "/appl_config/gauge_data/gauge_x"</param>
        /// <param name="value">{The value inside node_path.}</param>
        public void updateNodeData(string node_path, string value)
        {
            // ex. node_path = "/appl_config/gauge_data"; data will be updated inside note_path.
            XmlNode xNode = xDoc.SelectSingleNode(node_path);
            xNode.InnerText = value.Trim().Trim('\n');
            save();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node_path">Ex. "/appl_config"</param>
        /// <param name="attribute_name">"name"</param>
        /// <param name="value">{The value inside node_path.}</param>
        public void updateAttData(string node_path, string attribute_name, string value)
        {
            XmlNode xNode = xDoc.SelectSingleNode(node_path);
            xNode.Attributes[attribute_name].Value = value.Trim().Trim('\n');
            save();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xpath">Ex. "/appl_config"</param>
        /// <returns>{The value inside node_path.}</returns>
        public string readNodeData(string xpath)
        {
            read();
            string value = "";
            try
            {
                XmlNode xNode = xDoc.SelectSingleNode(xpath);
                value = xNode.InnerText.Trim().Trim('\n');
            }
            catch (System.Exception ex) { value = "Invalid XML Format." + ex.ToString(); }
            return value;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node_path">Ex. "/appl_config"</param>
        /// <param name="attribute_name">"name"</param>
        /// <returns>{The value inside node_path.}</returns>
        public string readAttData(string node_path, string attribute_name)
        {
            read();
            string value = "";
            try
            {
                XmlNode xNode = xDoc.SelectSingleNode(node_path);
                value = xNode.InnerText.Trim().Trim('\n');
            }
            catch (System.Exception ex) { value = "Invalid XML Format." + ex.ToString(); }
            return value;
        }
        public string timeNowString()
        {
            return DateTime.Now.ToString();
        }
    }
}
